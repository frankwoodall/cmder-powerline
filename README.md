# Installation

Place `powerline_main.lua` as well as the `powerline` directory in `%CMDER_ROOT%/config` and restart cmder.

# Dependencies

A font with powerline glyphs. Something from [Nerd Fonts](https://github.com/ryanoasis/nerd-fonts) will work nicely.