--------------------------------------------------------
-- This file provides all path related functionality
--------------------------------------------------------

local utils = require("powerline.utils")

local mod = {}


function mod.set_drive()
    local letter = clink.get_cwd()
    letter = string.sub(letter, 0, 1)
      
    local drive_letter = utils.fg(name_bg) .. utils.bg(drive_bg) .. sym_section_sep .. ' '
    drive_letter = drive_letter .. utils.fg(drive_fg) .. letter .. ' '
    drive_letter = drive_letter .. utils.fg(drive_bg) .. utils.bg(path_bg) .. sym_section_sep .. ' '
	clink.prompt.value = string.gsub(clink.prompt.value, '{drive}', drive_letter)
end

function mod.path_prompt_filter()
	local _cwd = clink.get_cwd()
    
    -- remove drive letter
    local cwd = string.sub(_cwd, 3)
       
	local len_cwd = string.len(cwd)	
	local sep = " "..sym_path_sep.." "
		
	-- Split the cwd and determine number of pieces
	local dir_pieces = clink.split(cwd, '\\')
	local num_pieces = #dir_pieces
	
	-- Some constants to construct dir path later
	local ROOT = 1
	local FIRST_DIR = 2
	local LAST_DIR = num_pieces
	
    -- Remove first slash
    cwd = string.gsub(cwd, '\\', '', 1)
    
	-- Replace all slashes with "sep"
	cwd = string.gsub(cwd, '\\', sep)
	
	-- If the cwd is > 40 characters AND has at least 3 pieces
	if len_cwd > 40 and num_pieces > 2 then
		cwd = dir_pieces[ROOT] .. sep ..
		      dir_pieces[FIRST_DIR] .. sep .. 
			  "..." .. sep .. 
			  dir_pieces[LAST_DIR]
	end
	
	-- Apply color and update the {cwd} placeholder
    cwd = utils.fg(path_fg) .. utils.bg(path_bg) .. cwd .. ' '
	clink.prompt.value = string.gsub(clink.prompt.value, '{cwd}', cwd)
end



-- Register module
return mod
