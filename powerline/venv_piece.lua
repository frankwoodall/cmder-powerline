--------------------------------------------------------
-- This file provides all venv related functionality
--------------------------------------------------------

local mod = {}


-- filter to handle virtual envs
function mod.set_venv_piece()
	local in_venv = false
	local venv_name = ''
	
	-- Determine if we're in a venv
	-- I'm assuming that if env var "VIRTUAL_ENV" is set, then we're in a venv
	if clink.get_env('VIRTUAL_ENV') then
		in_venv = true
	end
		
	if in_venv then
		-- Determine Name
		venv_name = clink.get_env('PROMPT'):match('.*%([%w-_]+%)')	
        venv_name = "\x1b[38;2;218;110;0m" .. venv_name 
	end
	
	clink.prompt.value = string.gsub(clink.prompt.value, "{venv}", venv_name .. " ")	
end

-- Register module
return mod

