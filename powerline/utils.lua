--------------------------------------------------------
-- This file provides all path related functionality
--------------------------------------------------------

local mod = {}

-- set background color by rgb value
function mod.rgb_bg(color)
    return "\x1b[48;2;"..color.."m"
end

-- set foreground color by rgb value
function mod.rgb_fg(color)
    return "\x1b[38;2;"..color.."m"
end

-- set foreground color by xterm 256 value
function mod.fg(color)
    return "\x1b[38;5;" .. color .. "m"
end

-- set background color by xterm 256 value
function mod.bg(color)
    return "\x1b[48;5;" .. color .. "m"
end

-- set foreground and background at same time by xterm 256 value
function mod.fgbg(fg_color, bg_color)
    return "\x1b[38;5;" .. fg_color .. ";48;5;" .. bg_color .. "m"
end


-- Register module
return mod
