--------------------------------------------------------
-- This file serves as the configuration file that determines
-- how the prompt will be draw, as well as various behavious
--------------------------------------------------------

--------------- Colors ----------------------------
-- Here's a good resource for colors in the terminal to explain
-- what all the \e[34m crap is
-- http://jafrog.com/2013/11/23/colors-in-terminal.html

------------- Colors ------------------------------
reset = "\x1b[0m"

--xterm 256 color codes
name_fg = 250
name_bg = 240
drive_fg = 15
drive_bg = 31
path_fg = 250
path_bg = 237
lambda_fg = 70
sep_fg = 244
venv_fg = 208

-- 24 bit RGB color codes
tip_rgb = "7;197;251"


git_colors = {
    clean_rgb = "160;235;50",
    dirty_rgb = "200;100;00"
}
--------------- End Colors ------------------------

--------------- Symbols, Glpyhs, etc. -------------
-- My name
my_name = "frank"

-- Symbol for end of prompt
prompt_tip = "λ"

-- separates path elements
sym_path_sep = ""

-- separates different sections of the prompt
sym_section_sep = ""

-- Git related
sym_branch = ""
git_start = ""
git_end = ""



-- unused/parking lot
local git_closingcolors = {
    clean = " \x1b[32;40m",
    dirty = "± \x1b[33;40m"
}

local a = ""
local c = ""
local c = ""
local d = ""
local e = ""
local f = ""