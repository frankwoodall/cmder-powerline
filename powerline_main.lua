-- Source: https://github.com/AmrEldib/cmder-powerline-prompt 

-- Add cmder/config directory to lua search path
package.path = package.path .. ';' .. clink.get_env('CMDER_ROOT') ..'\\config\\?.lua'

-- require helper files
local config = require("powerline.config")
local venv = require("powerline.venv_piece")
local git = require("powerline.git_piece")
local path = require("powerline.path_piece")
local utils = require("powerline.utils")


-- Need to load this file or we won't be able to detect virtual envs
local clink_lua_file = clink.get_env('CMDER_ROOT')..'\\vendor\\clink\\clink.lua'
dofile(clink_lua_file)



--------------- Filter Functions -----------------

function get_scaffold()
	local prompt = "{user}{drive}{cwd}{git}\n{venv}{tip}{rst} "
	clink.prompt.value = prompt
end

function setname()
    local name = utils.fgbg(name_fg, name_bg) .. " " .. my_name .. " " 
	clink.prompt.value = string.gsub(clink.prompt.value, '{user}', name)
end

-- filter to handle the tip
function tip_prompt_filter()
    local tip = utils.rgb_fg(tip_rgb) .. prompt_tip
	clink.prompt.value = string.gsub(clink.prompt.value, "{tip}", tip)
end


function endprompt()
	clink.prompt.value = string.gsub(clink.prompt.value, '{rst}', reset)	
end

-- Register filters
clink.prompt.register_filter(get_scaffold, 0)
clink.prompt.register_filter(setname, 1)
clink.prompt.register_filter(path.set_drive, 2)
clink.prompt.register_filter(venv.set_venv_piece, 20)
clink.prompt.register_filter(path.path_prompt_filter, 30)
clink.prompt.register_filter(git.colorful_git_prompt_filter, 40)
clink.prompt.register_filter(tip_prompt_filter, 50)
clink.prompt.register_filter(endprompt, 99)







